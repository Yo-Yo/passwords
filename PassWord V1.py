# -*- coding: utf-8 -*-
"""
Created on Fri Aug 22 11:46:18 2014

@author: alex
"""

import sys
import os

#Variables:
#File locations
dictionaryFile = ""
passwordFile = ""

#NGram stats
uniqueNgrams = 0
totalNgrams = 0

#NGram dictionary
ngramList = {}

#Dictionary stats
topPassword = ""
topWeight = 0

#Temporary dictionary file location
tempDicLoc = "tempDic.tmp"


#Methods
#Generate NGrams from password list and 
def genNgrams(passFile):
    global uniqueNgrams, totalNgrams
    for line in passFile:
        line = line.rstrip()
        for ngramLen in [2,3,4]:
            for i  in range (len(line) - ngramLen):
                totalNgrams += 1
                currComb = line[i:i+ngramLen]
                if(ngramList.has_key(currComb)):
                    ngramList[currComb] += 1
                else:
                    ngramList[currComb] = 1
                    uniqueNgrams += 1
#Read in a dictionary and rank all passwords
#Ouput all passwords to a temporary storage file                    
def rankDic(dicFile, tempFile):
    global topPassword, topWeight
    for line in dicFile:
        line = line.rstrip()
        tmpWeight = 0
        for ngramLen in [2,3,4]:
            for i  in range (len(line) - ngramLen):
                currComb = line[i:i+ngramLen]
                if(ngramList.has_key(currComb)):
                    tmpWeight += ngramList[currComb]
        tmpWeight /= len(line)
        if(tmpWeight > topWeight):
            topPassword = line
            topWeight = tmpWeight
        tempFile.write(line + "\t" + str(tmpWeight) + "\n")

#Order temporary dictionary
def ordDic():
    os.system("sort " + tempDicLoc + " -n -r -k2 | cut -d \"\t\" -f1 > sortedDic.txt")
    os.system("rm " + tempDicLoc)


#Main thread
#Read in args (passwordFile, dictionaryFile)
if(len(sys.argv) < 3 or len(sys.argv) > 5):
    print("Argument error:")
    print("PassWord.py <PasswordFile> <DictionaryFile> [TempDictionary]")
    sys.exit()
else:
    passwordFile = sys.argv[1]
    dictionaryFile = sys.argv[2]
    if(len(sys.argv) == 4):
        tempDicLoc = sys.argv[3]

print("Generating NGrams")
passwordList = open(passwordFile, "r")
genNgrams(passwordList)
passwordList.close()
print("NGrams generated")

print("Ranking dictionary")
dictionaryList = open(dictionaryFile, "r")
tempDictionary = open(tempDicLoc, "w")
rankDic(dictionaryList, tempDictionary)

ordDic()

print("Dictionary rank complete")


print("NGrams generated: " + str(uniqueNgrams) + "/" + str(totalNgrams))
print("Top password: " + topPassword + " with rank: " + str(topWeight))


        